from django.shortcuts import render, redirect  
from operation.forms import EmployeeForm
from operation.models import Employee
from django.contrib import messages

# Create your views here.
# Insert Data
def index(request):
    if request.method == "POST":
       form = EmployeeForm(request.POST)
       if form.is_valid():
           try:
               form.save()
               messages.success(request, "Data Has Been Inserted Sucessfully Done!")
               return redirect('/index')
           except:
               pass
    else:
     form=EmployeeForm()
     return render(request,'index.html',{'form':form})


# Show Data
def show(request):
    employees = Employee.objects.all()  
    return render(request,"show.html",{'employees':employees})  

 # Edit Data
def edit(request ,id):
    employee= Employee.objects.get(id=id)
    return render(request,'edit.html',{'employee':employee})

# Update Data
def update(request, id):  
    employee = Employee.objects.get(id=id)  
    form = EmployeeForm(request.POST, instance = employee)  
    if form.is_valid():  
        form.save()
        messages.success(request, "Data Has Been Updated Sucessfully Done!")  
        return redirect("/data")  
    return render(request, 'edit.html', {'employee': employee})  

# Destroy Or Delete Data
def destroy(request, id):
   employee=Employee.objects.get(id=id)
   employee.delete()
   messages.success(request, "Data Has Been Deleted Sucessfully Done!")
   return redirect("/data")