from django.contrib import admin
from django.urls import path
from operation import views  

urlpatterns = [
 path('index',views.index),
 path('data',views.show),
 path('edit/<int:id>', views.edit),
 path('update/<int:id>', views.update),
 path('delete/<int:id>', views.destroy),
]